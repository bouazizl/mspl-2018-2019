# MSPL-2018-2019

Modèles statistiques et Programmation Lettrée Licence MIAGE3 2018-2019

Welcome to the Literate Programming public repository, part of the
Miage L3.

The course is organized by sessions mixing fundamentals and activities

## Fundamentals
0. [General Introduction](./Presentations/0-Introduction/mspl-2019-0-introduction.pdf) - Course overview, goals, references and reproducibility
1. [Literate Programming](./Presentations/1-Litterate-Programming/mspl-2019-1-literate-programming.pdf) - Literate Programming Motivation & RStudio Case Study
2. [Data Representation](./Presentation/2-Visualization/main/Visualisation.2017.02.07.pdf) - Checklist for good graphics ([Slides for Discussion](./Presentation/2-Visualization/2-Visualization.pdf))
3. [Data Manipulation](./Presentation/3-Manipulation/3-Manipulation.pdf) - Using the dplyr R package to manipulate data
4. [Probability / Statistics](./Presentation/4-RevProbability/3_introduction_to_statistics_sel.pdf) - an Introduction to Probability and Statistics (revision)

## Activities
## Specifications for *Travaux dirigés*

1. [Git setup and kickoff](./Activities/TD1.espec.md)
2. [Critical view for plots](./Activities/TD2.espec.md)
3. [Using RStudio for running a Statistical Analysis](./TD3.espec.md)
4. [Combining RStudio and The Grammar of Graphics (of ggplot2)](./TD4.espec.md)
5. [Data Manipulation with dplyr](./TD5.espec.md)
6. [Galilée et le Paradoxe du Duc de Toscane](./TD6.espec.md)
7. [Hypothesis Test : le Paradoxe du Duc de Toscane (la suite)](./TD7/TD7.Rmd)

## Additional resources

- [RStudio Cheat Sheets](https://www.rstudio.com/resources/cheatsheets/)

## Contact Information

If you encounter any problem, please contact the professors by e-mail:
- Jean-Marc Vincent (jean-marc.vincent@imag.fr)
- Michael Mercier (adrien.faure@inria.fr)
